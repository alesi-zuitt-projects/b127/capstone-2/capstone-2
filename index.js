const express = require('express');
const mongoose = require('mongoose');
/* allows our backend to be available to our front end application
allows us to control app's cross origin resource sharing settings*/
const cors = require('cors');

//allow access to the routes

const userRoutes = require('./routes/userRoutes');
const shoeRoutes = require('./routes/productRoutes');
const adminRoutes = require('./routes/adminRoutes');
const orderRoutes = require('./routes/orderRoutes')

const app = express();


//connect to our mongodb
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.nyots.mongodb.net/batch127_capstone2-resurreccion?retryWrites=true&w=majority",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true

		}
);

// prompts message at the terminal
let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("we're connected to the mongodb atlas"));


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.use('/users', userRoutes);
app.use('/products', shoeRoutes);
app.use('/admins', adminRoutes);
app.use('/orders', orderRoutes);



app.listen(process.env.PORT || 4000,() => {
	console.log(`API is now up @ port ${process.env.PORT || 4000}`)
})