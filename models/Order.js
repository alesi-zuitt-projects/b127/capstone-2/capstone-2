

const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
    totalAmount: {
        type: Number,
    },
    products: [
        {
            productId: {
                type: String,
                required: [true, "productId is required."]
            },
            quantity:{
                type: Number,
                required: [true, "quantity is required."],
                default: 1
            },
            totalPerPair: {
				type: Number
			},
            purchasedOn: {
                type: Date,
                default: new Date()
            }

        }
    ],
    userId:{
        type: String,
        required:[true, "userId is required"]
    }
   
});

module.exports = mongoose.model("Order", orderSchema);
