const mongoose = require ('mongoose');

const adminSchema = new mongoose.Schema({


firstName: {
		type: String,
		required: [true, "firstName is required"]
	},
lastName: {
		type: String,
		required: [true, "lastName is required"]
	},
email: {
		type: String,
		required: [true, "email is required"]
	},
password: {
		type: String,
		required: [true, "password is required"]	
	},

mobileNo: {
		type: String,
		required: [true, "Mobile Number is required"]
	},
address: {
		type: String,
		required: [true, "Complete Address is required"]
},
isAdmin: {
		type: Boolean,
		default: true
	},
isActive: {
		type: Boolean,
		default: true
	}






})








module.exports = mongoose.model("Admin", adminSchema);