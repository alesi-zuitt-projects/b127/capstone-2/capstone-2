const mongoose = require ('mongoose');
const productSchema = new mongoose.Schema({

	brandName: {
		type: String,
		required: [true,"Brand Name is required"]
	},

	productName: {
		type: String,
		required: [true, "Product Name is required"]
	},
	size: {
		type: Number,
		required: [true, "Order Size is required"]
	},

	colorWay: {
		type: String,
		required: [true, "Color Way is required"]
	},

	price: {
		type: Number,
		required: [true, "Price is required"]
	},

	isActive: {
		type: Boolean,
		default: true,
	},

	dateCreated:{
		type: Date,
		default: new Date()
	}


})


module.exports = mongoose.model("Product", productSchema);