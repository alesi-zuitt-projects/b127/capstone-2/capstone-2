const express = require('express');
const router = express.Router();
const orderController = require ('../controllers/orderControllers');
const auth = require ('../auth');


router.post('/addOrder', auth.verify, (req,res) =>{
	const data = {
		order:req.body,
		userId: auth.decode(req.headers.authorization).id,
	}
	if(data.userId){
	orderController.addtoCart(data).then(result => res.send(result))
	}else{
		res.send(false)
	}
})

router.get('/myOrders', auth.verify, (req,res) => {	
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.userId){
		orderController.getMyOrders(data).then(result => res.send(result))
	}else{
		res.send(false)
	}
})



module.exports = router;