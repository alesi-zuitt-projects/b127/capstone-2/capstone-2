const express = require('express');
const router = express.Router();
const productController = require ('../controllers/productControllers');
const auth = require ('../auth');

//create product
router.post('/createProduct', auth.verify, (req,res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
	productController.addProduct(data).then(result => res.send(result))
	}else{
		res.send("Only admins can create product!!!")
	}
})

//Retrieve all active products
router.get('/active', (req, res) =>{
	productController.getAllActive().then(result => res.send(result))
})



//getAllProducts
router.get('/all', (req,res) => {
	productController.getProducts().then(result => res.send(result))
})

//findById Product
router.get('/:productId', (req,res) => {
	productController.getProductById(req.params).then(result => res.send(result))
})

//update

router.put('/:productId/update', auth.verify, (req,res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if (data.isAdmin){
	productController.updateProduct(req.params, req.body).then(result => res.send(result))
}else{
	res.send("Only admins can update products!!!")
} 
})

//Archive
router.put('/:productId/archive', auth.verify, (req,res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	
if(data.isAdmin){
	productController.archiveProduct(req.params).then(result => res.send(result))
}else{
	res.send("Only admins can archive products!!!")
}
})

//activate
router.put('/:productId/activate', auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		productController.activateProduct(req.params).then(result => res.send(result))
	}else{
		res.send(false)
	}
})



module.exports = router;