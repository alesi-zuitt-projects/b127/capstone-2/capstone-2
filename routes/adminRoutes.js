const express = require('express');
const router = express.Router();
const adminController = require ('../controllers/adminControllers');
const userController = require ('../controllers/userControllers')
const auth = require ('../auth');

//register
router.post('/registrationAdmin', (req,res) => {
	adminController.registerAdmin(req.body).then(result => res.send(result))
})
//login
router.post('/loginAdmin', (req, res) =>{
	adminController.loginAdmin(req.body).then(result => res.send(result));
})
//getAdmin
router.get('/getAdmins', (req,res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
	adminController.getAllAdmin().then(result => res.send(result))
	}else{
		res.send("Only admins can get admin lists!!!")
	}
})
//getall users
router.get('/getUsers', auth.verify,(req,res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
	adminController.getAllUser().then(result => res.send(result))
	}else{
		res.send("Only admins can get all users!!!")
	}
})

//set users as an admin
router.put('/:userId/set-admin', auth.verify, (req,res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
		adminController.setAdmin(req.params).then(result => res.send(result))
	}else{
		res.send("Only admins can set normal user as an admin!!!")
	}
})

//retrive all orders
router.get('/getAllOrders', auth.verify, (req,res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if (data.isAdmin){
		adminController.getAllOrder().then(result => res.send(result))
	}else{
		res.send("Only admins can retrieve all orders!!!")
	}
})

//get user by id
router.get('/:id', auth.verify, (req,res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
	adminController.getUser(req.params.id).then(result => res.send(result));
	}else{
		res.send("Only admins can access users!!!")
	}
})

//delete user by id
router.delete('/:id/user', auth.verify, (req,res) => {
const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if (data.isAdmin){
	adminController.deleteUser(req.params.id).then(result => res.send(result))
	}else{
	res.send("Only admins can delete another users")
	}
})
//delete product by id
router.delete('/:id/product', auth.verify, (req,res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
		adminController.deleteProduct(req.params.id).then(result => res.send(result))
	}else{
		res.send("Only admins can delete the products")
	}
})



module.exports = router;