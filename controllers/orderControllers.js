const Product = require ('../models/Product');
const Order = require ('../models/Order');


// add to cart
module.exports.addtoCart = async (data) => {

	let totalPriceOfAll = 0;
	let productDetail = await Product.findById(data.order.productId);
	let isOrderExist = await Order.findOne({userId: data.userId});
	
	if (isOrderExist) {
		let productIndex = isOrderExist.products.findIndex(product => product.productId == data.order.productId);

		if (productDetail.isActive) {

			if (productIndex > -1) {

				let product = isOrderExist.products[productIndex];
				product.quantity = data.order.quantity;
				product.totalPerPair = product.quantity*productDetail.price;
			}else {

				let addProduct = {
					productId: data.order.productId,
					quantity: data.order.quantity,
					
					totalPerPair: productDetail.price*data.order.quantity
				}
				isOrderExist.products.push(addProduct);
			} 
		}else {
			return false;
		}

	for (i = 0; i < isOrderExist.products.length; i++) {
		totalPriceOfAll += isOrderExist.products[i].totalPerPair;
	}

	isOrderExist.totalPrice = totalPriceOfAll 
	isOrderExist = await isOrderExist.save();
	return true;

	}
	
	else {
		let addProduct = [];

		if (productDetail.isActive) {

			let productJSON = {
				productId: data.order.productId,
				quantity: data.order.quantity,
				
				totalPerPair: productDetail.price*data.order.quantity
			}
			await addProduct.push(productJSON);
			let newOrder = new Order ({
				products: addProduct,
				userId: data.userId,
				totalPrice: productDetail.price*data.order.quantity
			});

			return newOrder.save().then((result,isError) => {
				return (isError)? false : true;
			})
		} else {
			return false;
		}
	}
}

/*module.exports.getAllOrders = (data) => {
	return Order.find({userId: data.userId}).then(result => {
		return result
	})
}*/


module.exports.getMyOrders = (data) => {
	return Order.find({userId: data.userId}).then(result =>{
		let orderArr	=	[];
		result.forEach(order	=>	{
			orderArr.push(order.products)
		})
		return orderArr.flat();
	});
}