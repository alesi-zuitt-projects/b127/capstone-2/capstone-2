const User = require ('../models/User');
const Order = require ('../models/Order')
const Shoe = require ('../models/Product')
const Admin = require ('../models/Admin')
const bcrypt = require('bcrypt');
const auth = require('../auth')

module.exports.checkEmailExists = (reqBody) => {
	return User.find( { email: reqBody.email } ).then(result => {

		//The "find" mthod returns a record if a match is found
		if(result.length > 0){
			return true;
		}else{
			//No duplicate email found
			//The user is not yet registered in the database
			return false;
		}
	})
}



module.exports.getProfile = (data) => {
    return User.findById(data._id).then(result => {
        result.password = "";
        return result;
    })
}

//register user
module.exports.registerUser = (reqBody) => {
	return User.find ({ email: reqBody.email }).then((result) => {
		if(result.length > 0){
			return ("Username Already Exist!!!")
		}
		else{
			let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		address: reqBody.address,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
		})
			return newUser.save().then((user, error) => {
		//error
		if(error){
			return ("Please make sure that you entered details correctly");
		}else {
			return user;
		}
		})
	}
})
	
};




//loginm user
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		//if user does not exist
		if (result == null){
			return false;
		}else {
			
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if (isPasswordCorrect){
				
				return {	accessToken: auth.createAccessToken(result.toObject())	}
			}else{
				
				return false;
			}
		}
	})
};

//getall users
module.exports.getAllUser = () => {
	return User.find().then(result => {
		return result
	})
}

//getindividual for admin
module.exports.getProfile = (reqParams) => {
	return User.findById(reqParams).then(result => {
		return result;
	})
}


//getorder per user

module.exports.getUserOrders = (reqParams) => {
	return User.findById(reqParams.userId).then((result,error) => {
		if(error){
			return false
		}else{
			return result.orders
		}
	})
} 



//username:alesi123
//password: alesi1234