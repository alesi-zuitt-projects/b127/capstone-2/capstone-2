const Product = require ('../models/Product')

//create product
module.exports.addProduct = (data) => {
		let newProduct= new Product ({
			brandName: data.product.brandName,
			productName: data.product.productName,
			colorWay: data.product.colorWay,
			size: data.product.size,
			isAvailable: data.product.isAvailable,
			price: data.product.price
		})
		
		return newProduct.save().then((products,error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})
}

//getall active products
module.exports.getAllActive= () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
}



//getProducts
module.exports.getProducts = () => {
	return Product.find().then(result =>{
		return result
	})
}

//getProductById
module.exports.getProductById = (reqParams) =>{
	return Product.findById(reqParams.productId).then(result => {
		return result
	})
}

//Update product
module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		brandName: reqBody.brandName,
		productName: reqBody.productName,
		size: reqBody.size,
		colorWay: reqBody.colorWay,
		isActive: reqBody.isActive,
		price: reqBody.price
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product,error) => {
		if (error){
			return false;
		}else{
			return true;
		}
	})
}

//Archive

module.exports.archiveProduct = (reqParams) => {
		let updatedProduct = {
			isActive: false
		} 
		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product,error) =>{
			if(error){
				return false;
			}else{
				return true;
			}
		})
	}


//Activate
module.exports.activateProduct = (reqParams) => {

	let updatedProduct = {
		isActive: true
	}
	
	//findByIdAndUpdate(ID, updatesToBeApplied)
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}